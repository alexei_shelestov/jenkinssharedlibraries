def validate() {
	if (!fileExists('README.md') || !fileExists('CHANGELOG.md')) { 
		error 'Readme or CHANGELOG not found' 
	}		
}


def init() {
	env.PATH = "${tool 'USTA_Maven'}/bin:${env.PATH}"	
}	

def build(maven_postfix) {
	sh "mvn clean install ${maven_postfix} -DskipTests"
	archiveArtifacts artifacts: '**/target/*.zip', fingerprint: true
	stash includes: '**/target/*.zip', name: 'artifacts'
}