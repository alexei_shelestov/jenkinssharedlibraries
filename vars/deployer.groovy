def clearDispatcherCache(conf, targetEnv) {
	echo 'Clearing cache on dispatchers'
	def cache_clear_requests = [:]
				  
	conf.ecosystem.envs["${targetEnv}"].web.each{ env_name, env_object -> 
		cache_clear_requests["Clearing cache thread '${env_name}'"] = { 
			sh "/usr/bin/curl -H \"CQ-Action: DELETE\" -H \"CQ-Handle:/\" -H \"Content-Length: 0\" -H \"Content-Type: application/octet-stream\" http://${env_object.host}/dispatcher/invalidate.cache"
		}
	}
						
	parallel cache_clear_requests		
}



def deployToAEMInstance(env_name, env_object, conf) { 	
	echo 'Deploy artifact to AEM intance'
	withCredentials([usernamePassword(credentialsId: "${env_object.credentialsId}", passwordVariable: 'password', usernameVariable: 'username')]) {							
		dir(conf.aemDeploymentContext){
			sh "mvn content-package:install -Dcrx.host='${env_object.host}' -Daem.host='${env_object.host}' -Daem.port='${env_object.port}'  -Dcrx.port='${env_object.port}'  -Dvault.targetURL='http://${env_object.host}:${env_object.port}/crx/packmgr/service.jsp' -Dvault.userId='${username}' -Dvault.password='${password}' -Dcrx.username='${username}' -Dcrx.password='${password}' ${conf.mavenPostfix}"			
		}	
	}			
}

def deployMuleToNexusAndArm(maven_postfix, targetEnv) {
	echo "Deploy artifact to Mule instance ${targetEnv}"
	if(targetEnv == 'STAGE' || targetEnv == 'PRODUCTION'){
		sh "mvn deploy -P${targetEnv}1 -Dmaven.test.skip=true -Dmaven.package.skip=true  " +
				"-Dmaven.install.skip=true ${maven_postfix}"
		sh "mvn deploy -P${targetEnv}2 -Dmaven.test.skip=true -Dmaven.package.skip=true  " +
				"-Dmaven.install.skip=true ${maven_postfix}"
	} else{
		sh "mvn deploy -P${targetEnv} -Dmaven.test.skip=true -Dmaven.package.skip=true  " +
				"-Dmaven.install.skip=true ${maven_postfix}"
	}
}

def deployToAEMInstances(conf, targetEnv) { 	
		def deployments = [:]
		  
		conf.ecosystem.envs["${targetEnv}"].app.each{ env_name, env_object -> 
			deployments["Deploy thread ${env_name}"] = { 
				deployToAEMInstance(env_name, env_object, conf)
			}
		}
							
		parallel deployments		
}

def deployToNexus(maven_postfix) { 	
	echo 'Deploy artifacts to Nexus repository'
	sh "mvn deploy -Dmaven.test.skip=true -Dmaven.package.skip=true  -Dmaven.install.skip=true ${maven_postfix}"	
}


def shouldDeploy(targetEnv) {
	return targetEnv != null && targetEnv != 'none'
}