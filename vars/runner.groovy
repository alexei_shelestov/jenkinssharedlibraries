def startPipeline(params, conf) {
    try {
        utils.generateInputFields(conf.inputProps)

        if (shouldBeAutoDeployed(conf, params)) {
            handleAutoDeploy(conf, params)
        } else {
            runBuildPipeline(params, conf)
            runDeployToNexusStage(conf)
            runDeployToAEMStage(conf, params.target_env)
            runPostDeployStage(conf, params.target_env)
            if (deployer.shouldDeploy(params.target_env)) {
                notifier.notifySuccess(conf.ecosystem)
            }
        }
    } catch (e) {
        notifier.notifyFailure(conf.ecosystem)
        throw e
    } finally {
        deleteDir() /* clean up our workspace */
    }
}

def startMulePipeline(params, conf) {
    try {
        utils.generateInputFields(conf.inputProps)

        runBuildPipeline(params, conf)
        if (deployer.shouldDeploy(params.target_env)) {
            deployMuleToNexusAndArm(conf, params.target_env)
            runIntegrationTestsPipeline(params)
        }
      //  notifier.notifySuccess(conf.ecosystem)
    } catch (e) {
      //  notifier.notifyFailure(conf.ecosystem)
        throw e
    }
    finally {
        deleteDir() /* clean up our workspace */
    }
}

def startTestMulePipeline(params, inputProps, ecosystem) {
    try {
        utils.generateInputFields(inputProps)
        stage('Init') {
            builder.init()
        }
        stage('Checkout integration tests') {
            checkout scm
        }
        stage('Bulid integration tests') {
            sh "mvn clean install -DskipTests -Dcheckstyle.skip"
        }
        stage('Run api integration tests - MuleTokenTest') {
            runSpecificTest(params, "com.usta.api.test.token.MuleTokenTest")
        }
        stage('Run api integration tests - HybrisEndpointsTest') {
            runSpecificTest(params, "com.usta.api.test.hybris.HybrisEndpointsTest")
        }
        stage('Run api integration tests - HybrisEndpointsNologinTest') {
            runSpecificTest(params, "com.usta.api.test.hybris.HybrisEndpointsNologinTest")
        }
        stage('Run api integration tests - SolrTest') {
            runSpecificTest(params, "com.usta.api.test.solr.SolrTest")
        }

        notifier.notifySuccess(ecosystem)
    } catch (e) {
        notifier.notifyFailure(ecosystem)
        throw e
    }
    finally {
        junit 'api/target/surefire-reports/*.xml'
        deleteDir() /* clean up our workspace */
    }
}


def startDockerPipeline(params, ecosystem, service) {
    try {
        withMaven(maven: 'USTA_Maven') {
            stage('Build package') {
                if (params.skip_tests) {
                    sh 'mvn clean package -DskipTests'
                } else {
                    sh 'mvn clean package'
                }
            }
            stage('SonarQube analysis') {
                withSonarQubeEnv('sonarqube') {
                    sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar'
                }
            }
        }
        stage('Run Docker Container') {
            if (params.target_env != null && params.target_env != 'none') {
                def environment = ecosystem.envs["${params.target_env}"]
                def safesport = environment["${service}"]
                def javaWorkspace = safesport.javaWorkspace
                def dockerWorkspace = safesport.dockerWorkspace
                def port = safesport.port
                def debugPort = safesport.debugPort
                def serviceName = safesport.serviceName
                def internalPort = safesport.internalPort
                def dockers = safesport.docker.split(',')

                sh 'docker build --tag=' + serviceName + ':latest --rm=true .'
                sh 'docker save -o ' + serviceName + '.tar ' + serviceName

                for (def docker : dockers) {
                    sh 'scp ' + serviceName + '.tar ubuntu@' + docker + ':/home/ubuntu'
                    sh 'ssh ubuntu@' + docker + ' "docker load -i ' + serviceName + '.tar"'
                    try {
                        sh 'ssh ubuntu@' + docker + ' "docker stop ' + serviceName + '"'
                    } catch (e) {
                        e.printStackTrace()
                    }
                    try {
                        sh 'ssh ubuntu@' + docker + ' "docker rm ' + serviceName + '"'
                    } catch (e) {
                        e.printStackTrace()
                    }
                    sh 'ssh ubuntu@' + docker + ' "docker run -d --name=' + serviceName + ' --restart always --publish=' + port + ':' + internalPort + ' --publish=' + debugPort + ':8000 -v ' + dockerWorkspace + ':' + javaWorkspace + ' ' + serviceName + ':latest"'
                }
            }
        }
        notifier.notifySuccess(ecosystem)
    } catch (e) {
        notifier.notifyFailure(ecosystem)
        throw e
    }
    finally {
        deleteDir() /* clean up our workspace */
    }
}

def startDockerMaintenancePipeline(params, dockerEcosystem) {
    try {
        stage('Run Docker Command') {
            if (params.target_env != null && params.target_env != 'none') {
                println("Params: " + params)

                def environment = dockerEcosystem["${params.target_env}"]
                def docker = environment.docker
                def serviceName = environment.serviceName

                def shCommand = 'ssh ubuntu@' + docker + ' "docker ' + params.command + ' ' + serviceName + '"'
                println("sh command: " + shCommand)
                sh shCommand
            }
        }
        notifier.notifySuccess(dockerEcosystem)
    } catch (e) {
        notifier.notifyFailure(dockerEcosystem)
        throw e
    }
}


def runSpecificTest(params, testName) {
    sh "mvn test -Dtest=${testName} -Dcfg.env.name=${params.target_env} -Dcheckstyle.skip -DfailIfNoTests=false"
}

def runPostDeployStage(conf, targetEnv) {
    if (deployer.shouldDeploy(targetEnv)) {
        stage("Post-deploy (${targetEnv})") {
            deployer.clearDispatcherCache(conf, targetEnv)
        }
    }
}

def runDeployToNexusStage(conf) {
    stage('Deploy to Nexus') {
        unstash 'artifacts'
        deployer.deployToNexus(conf.mavenPostfix)
    }
}

def runIntegrationTestsPipeline(params) {
    if (!params.skip_tests) {
        stage('Run integration tests') {
            build job: 'RunMuleIntegrationTests', propagate: true, wait: true, parameters: [[$class: 'StringParameterValue', name: 'target_env', value: params.target_env]]
        }
    }
}


def runBuildPipeline(params, conf) {
    stage('Init') {
        builder.init()
    }
    stage('Checkout sources') {
        checkout scm
    }
    stage('Validate') {
        builder.validate()
    }

    stage('Build') {
        builder.build(conf.mavenPostfix)
    }

    parallel(
            "Unit Tests ": {
                stage('Test') {
                    if (Boolean.valueOf(params.skip_tests)) {
                        echo "Test stage was skipped"
                    } else {
                        try {
                            sh "mvn test"
                        }
                        finally {
                            junit '**/surefire-reports/*.xml'
                        }
                    }
                }
            },
            "Static Code Analysis": {
                stage('SonarQube analysis') {
                    if (Boolean.valueOf(params.skip_tests)) {
                        echo "Test stage was skipped"
                    } else {
                        withSonarQubeEnv {
                            sh "mvn sonar:sonar ${conf.mavenPostfix}" +
                                    ' -Dsonar.projectKey=' + conf.sonar.projectKey +
                                    ' -Dsonar.projectName=' + conf.sonar.projectName +
                                    ' -Dsonar.projectVersion=2.0' +
                                    ' -Dsonar.exclusions=**/test/**' +
                                    ' -Dsonar.sources=.' +
                                    ' -Dsonar.java.source=1.8' +
                                    ' -Dsonar.java.target=1.8'
                        }
                    }
                }
            }
    )
}

def deployMuleToNexusAndArm(conf, targetEnv) {
    stage("Deploy Mule to Nexus and Mule server") {
        unstash 'artifacts'
        deployer.deployMuleToNexusAndArm(conf.mavenPostfix, targetEnv)
    }
}

def runDeployToAEMStage(conf, targetEnv) {
    if (deployer.shouldDeploy(targetEnv)) {
        stage("Deploy to AEM (${targetEnv})") {
            unstash 'artifacts'
            deployer.deployToAEMInstances(conf, targetEnv)
        }
    }
}

def shouldBeAutoDeployed(conf, params) {
    def currentBranchIsInBuildTriggers = conf.ecosystem.buildTriggers.any { element -> env.BRANCH_NAME == element.branch }
    return conf.ecosystem.buildTriggers != null && (params.target_env == null || params.target_env == 'none') && currentBranchIsInBuildTriggers
}

def handleAutoDeploy(conf, params) {
    runBuildPipeline(params, conf)
    runDeployToNexusStage(conf)

    def autoDeploy = [:]

    conf.ecosystem.buildTriggers.each {
        if (env.BRANCH_NAME == it.branch) {
            autoDeploy["Env thread ${it.targetEnv}"] = {
                runDeployToAEMStage(conf, it.targetEnv)
                runPostDeployStage(conf, it.targetEnv)
            }
        }
    }

    parallel autoDeploy
    notifier.notifySuccess(conf.ecosystem)
}			