# JenkinsSharedLibraries

Shared libs for jenkins builds. Files are configured in Jenkins as usta-utils-mule. If you would ike to use them in Jenkinsfile please add at the beginning:

```groovy
@Library('usta-utils-mule') _
```